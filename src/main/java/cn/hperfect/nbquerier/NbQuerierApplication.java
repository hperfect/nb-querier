package cn.hperfect.nbquerier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NbQuerierApplication {

	public static void main(String[] args) {
		SpringApplication.run(NbQuerierApplication.class, args);
	}

}
