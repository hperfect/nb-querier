package cn.hperfect.nbquerier.core.component.executor;

import cn.hperfect.nbquerier.core.NbQuerier;
import cn.hperfect.nbquerier.core.component.convert.ITypeConvertor;
import cn.hperfect.nbquerier.core.component.datasouce.NbDataSource;
import cn.hperfect.nbquerier.core.component.result.IResultSetHandler;
import cn.hperfect.nbquerier.core.metedata.QueryValParam;
import cn.hperfect.nbquerier.exceptions.NbSQLException;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.datasource.ConnectionHolder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 执行器-> mybatis -> 执行?
 * 拿到base mapper执行
 * 重写result handler
 * <p>
 * 直接获取DataSource,获取连接，准备，执行 管理事务
 *
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 10:50 上午
 */
@Log4j2
public class DefaultNbExecutor implements INbExecutor {
    @Resource
    NbDataSource dataSource;
    @Resource
    IResultSetHandler resultSetHandler;
    @Resource
    PlatformTransactionManager transactionManager;

    @Override

    public ResultSet select(NbQuerier<?> querier) {
        try {
            Connection connection = dataSource.getConnection();
            String sql = querier.buildQuerySql();
            //替换文化符输出
            logSql(sql, querier);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            //设置参数
            List<QueryValParam> params = querier.getParams();
            if (CollUtil.isNotEmpty(params)) {
                for (int i = 0; i < params.size(); i++) {
                    QueryValParam param = params.get(i);
                    Assert.notNull(param.getType(), "设置参数的type不能为空");
                    preparedStatement.setObject(i + 1, param.getType().getConvert().convert(param.getValue()));
                }
            }
            return preparedStatement.executeQuery();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            throw new NbSQLException(throwable);
        }
    }

    private void logSql(String sql, NbQuerier<?> querier) {
        log.info("query sql:" + sql);
    }

    @Override
    public <T> List<LinkedHashMap<String, Object>> selectMapList(NbQuerier<T> querier) {
        try {
            ResultSet select = select(querier);
            return resultSetHandler.toMap(select);
        } catch (Throwable throwable) {
            throw new NbSQLException(throwable);
        }
    }

    @Override
    public int insert(NbQuerier<?> querier) {
        try {
            Connection connection = doGetConnection(dataSource);
            String sql = "INSERT INTO cus_qiyq.test_table (id, name) VALUES (112, '122')";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            return preparedStatement.executeUpdate();
        } catch (Throwable throwable) {
            throw new NbSQLException(throwable);
        }
    }


    protected static Connection doGetConnection(DataSource dataSource) throws SQLException {
        ConnectionHolder conHolder = (ConnectionHolder) TransactionSynchronizationManager.getResource(dataSource);
        if (conHolder != null && conHolder.isSynchronizedWithTransaction()) {
            conHolder.requested();
            return conHolder.getConnection();
        }
        return dataSource.getConnection();
    }


}
