package cn.hperfect.nbquerier.core.component.convert;

import cn.hperfect.nbquerier.core.metedata.BaseNbField;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/3 1:47 下午
 */
public enum NoTypeConvertor implements ITypeConvertor {
    /**
     * 实例
     */
    INSTANCE;

    @Override
    public Object convert(BaseNbField BaseNbField, Object value) {
        return value;
    }
}
