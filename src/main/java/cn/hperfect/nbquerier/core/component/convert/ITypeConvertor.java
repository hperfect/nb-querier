package cn.hperfect.nbquerier.core.component.convert;

import cn.hperfect.nbquerier.core.metedata.BaseNbField;
import cn.hperfect.nbquerier.exceptions.TypeConvertException;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/3 1:39 下午
 */
public interface ITypeConvertor{
    /**
     * 类型转换
     *
     * @param baseNbField
     * @param value
     * @return
     * @throws TypeConvertException
     */
    Object convert(BaseNbField baseNbField, Object value) throws TypeConvertException;

    default Object convert(Object value) throws TypeConvertException {
        return convert(null, value);
    }
}
