package cn.hperfect.nbquerier.core.component.convert;

import cn.hperfect.nbquerier.core.metedata.BaseNbField;
import cn.hperfect.nbquerier.exceptions.TypeConvertException;
import cn.hutool.json.JSONUtil;
import org.postgresql.util.PGobject;

import java.sql.SQLException;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/3 1:42 下午
 */
public enum JsonBTypeConvertor implements ITypeConvertor {
    /**
     * 实例
     */
    INSTANCE;

    @Override
    public Object convert(BaseNbField baseNbField, Object value) throws TypeConvertException {
        PGobject pGobject = new PGobject();
        pGobject.setType("jsonb");
        try {
            pGobject.setValue(JSONUtil.toJsonStr(value));
        } catch (SQLException e) {
            throw new TypeConvertException(String.format("字段%s类型错误", baseNbField.getName()));
        }
        return pGobject;
    }

}
