package cn.hperfect.nbquerier.core.component.builder;

import cn.hperfect.nbquerier.core.NbQuerier;
import cn.hperfect.nbquerier.core.metedata.NbQueryInfo;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 9:51 上午
 */
public interface INbQueryBuilder {
    /**
     * 构建query对象
     *
     * @param clazz
     * @param <T>
     * @return
     */
    <T> NbQuerier<T> build(Class<T> clazz);

    /**
     * 解析tableInfo
     *
     * @param tableClazz
     * @return
     */
    NbQueryInfo parseTableInfo(Class<?> tableClazz);
}
