package cn.hperfect.nbquerier.core.component.convert;

import cn.hperfect.nbquerier.exceptions.TypeConvertException;
import cn.hutool.core.convert.Convert;
import cn.hperfect.nbquerier.core.metedata.BaseNbField;

import java.util.List;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/3 1:42 下午
 */
public enum LongListTypeConvertor implements ITypeConvertor {
    /**
     * 实例
     */
    INSTANCE;

    @Override
    public Object convert(BaseNbField BaseNbField, Object value) throws TypeConvertException {
        return convert(value);
    }

    @Override
    public Object convert(Object value) throws TypeConvertException {
        if (value == null) {
            return null;
        }
        if (value.getClass().isArray()) {
            return value;
        }
        if (value instanceof List) {
            Object[] objects = ((List<?>) value).toArray();
            long[] longValue = new long[objects.length];
            for (int i = 0; i < objects.length; i++) {
                longValue[i] = Convert.toLong(objects[i]);
            }
            return longValue;
        }
        return value;
    }

}
