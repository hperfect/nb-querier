package cn.hperfect.nbquerier.core.component.convert;

import cn.hutool.core.date.DateUtil;
import cn.hperfect.nbquerier.core.metedata.BaseNbField;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/3 1:42 下午
 */
public enum DateTypeConvertor implements ITypeConvertor {
    /**
     * 实例
     */
    INSTANCE;

    @Override
    public Object convert(BaseNbField baseNbField, Object value) {
        if (value == null) {
            return null;
        }

        if (value instanceof LocalDateTime || value instanceof LocalDate) {
            return value;
        }
        //时间格式装换
        return DateUtil.parse(value.toString()).toJdkDate();
    }

}
