package cn.hperfect.nbquerier.core.component.convert;

import cn.hperfect.nbquerier.exceptions.TypeConvertException;
import cn.hperfect.nbquerier.core.metedata.BaseNbField;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/3 1:42 下午
 */
public enum ErrorTypeConvertor implements ITypeConvertor {
    /**
     * 实例
     */
    INSTANCE;

    @Override
    public Object convert(BaseNbField baseNbField, Object value) throws TypeConvertException {
        throw new TypeConvertException("没有类型转换器");
    }

}
