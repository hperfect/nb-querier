package cn.hperfect.nbquerier.core.component.builder.impl;

import cn.hperfect.nbquerier.annotation.NbField;
import cn.hperfect.nbquerier.annotation.NbTable;
import cn.hperfect.nbquerier.annotation.TableId;
import cn.hperfect.nbquerier.config.NbQuerierProperties;
import cn.hperfect.nbquerier.core.NbQuerier;
import cn.hperfect.nbquerier.core.component.builder.INbQueryBuilder;
import cn.hperfect.nbquerier.core.component.executor.INbExecutor;
import cn.hperfect.nbquerier.core.component.result.IResultSetHandler;
import cn.hperfect.nbquerier.core.impl.DefaultNbQuerier;
import cn.hperfect.nbquerier.core.metedata.BaseNbField;
import cn.hperfect.nbquerier.core.metedata.NbQueryInfo;
import cn.hperfect.nbquerier.core.metedata.PrimaryKey;
import cn.hperfect.nbquerier.enums.NbFieldType;
import cn.hperfect.nbquerier.enums.NbQueryType;
import cn.hutool.cache.Cache;
import cn.hutool.cache.CacheUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 9:52 上午
 */
public class DefaultNbQueryBuilder implements INbQueryBuilder {
    @Resource
    INbExecutor nbExecutor;
    @Resource
    IResultSetHandler resultSetHandler;
    @Resource
    NbQuerierProperties config;

    private final static Cache<Class<?>, NbQueryInfo> TABLE_INFOS = CacheUtil.newLFUCache(100);

    @Override
    public <T> NbQuerier<T> build(Class<T> clazz) {
        DefaultNbQuerier<T> querier = new DefaultNbQuerier<>(nbExecutor);
        querier.setQueryInfo(parseTableInfo(clazz));
        querier.setResultClass(clazz);
        return querier;
    }

    @Override
    public NbQueryInfo parseTableInfo(Class<?> tableClazz) {
        Assert.notNull(tableClazz, "table不能为空");
        NbQueryInfo queryInfo = TABLE_INFOS.get(tableClazz);
        if (queryInfo == null || !config.isClassCache()) {
            //构建tableInfo
            queryInfo = new NbQueryInfo();
            NbTable nbTable = tableClazz.getAnnotation(NbTable.class);
            if (nbTable != null) {
                if (StrUtil.isNotBlank(nbTable.value())) {
                    queryInfo.setSql(nbTable.value());
                }
                queryInfo.setSchema(nbTable.schema());
                queryInfo.setDs(nbTable.ds());
            } else {
                queryInfo.setSql(tableClazz.getSimpleName());
            }
            if (config.isTableNameToUnderlineCase()) {
                queryInfo.setSql(StrUtil.toUnderlineCase(queryInfo.getSql()));
            }
            queryInfo.setFields(parseFields(queryInfo, tableClazz));
            //解析
            //parsePk(tableInfo, tableClazz);
            //解析字段
            //tableInfo.setFields(FieldGenerator.parseToNbFields(tableClazz));

            if (config.isClassCache()) {
                TABLE_INFOS.put(tableClazz, queryInfo);
            }
            return queryInfo;

        }
        return queryInfo;
    }


    /**
     * 解析实体为NbField 之后无需反射解析
     *
     * @param queryInfo
     * @param tableClazz
     * @return
     */
    public List<BaseNbField> parseFields(NbQueryInfo queryInfo, Class<?> tableClazz) {
        List<BaseNbField> nbTableFields = new ArrayList<>();
        Field[] tableFields = tableClazz.getDeclaredFields();
        if (CollUtil.isNotEmpty(Arrays.asList(tableFields))) {
            for (Field field : tableFields) {
                BaseNbField baseNbField = new BaseNbField();
                //忽略final,static属性
                int modifiers = field.getModifiers();
                if (Modifier.isStatic(modifiers) || Modifier.isFinal(modifiers)) {
                    continue;
                }
                NbField nbField = field.getAnnotation(NbField.class);
                if (nbField != null && !nbField.exist()) {
                    continue;
                }
                TableId tableId = field.getAnnotation(TableId.class);
                //获取字段名称
                String fieldName;
                if (nbField != null && StrUtil.isNotBlank(nbField.value())) {
                    fieldName = nbField.value();
                } else {
                    fieldName = field.getName();
                    if (config.isFieldNameToUnderlineCase()) {
                        fieldName = StrUtil.toUnderlineCase(fieldName);
                    }
                }
                baseNbField.setFieldType(NbFieldType.NORMAL);
                if (tableId != null) {
                    if (StrUtil.isNotBlank(tableId.value())) {
                        fieldName = tableId.value();
                    }
                    PrimaryKey primaryKey = new PrimaryKey(fieldName, tableId.type(), tableId.queryType());
                    queryInfo.setPk(primaryKey);
                    baseNbField.setFieldType(NbFieldType.PK);
                }
                baseNbField.setName(fieldName);
                baseNbField.setQueryType(NbQueryType.convertFromClass(field.getType()));
                //数据库查询名称

                //约束获取
              /*  Constraint annotation = field.getAnnotation(Constraint.class);
                if (annotation != null) {
                    List<ValidateRule> validateRules = new ArrayList<>();
                    if (annotation.notNull()) {
                        validateRules.add(ValidateRule.NOT_NULL);
                    }
                    if (annotation.notUnique()) {
                        validateRules.add(ValidateRule.UNIQUE);
                    }
                    nbTableField.setValidateRules(validateRules);
                }
                //软删除字段
                String deleteField = TableInfoUtils.parseTableSoftDeleteField(tableClazz);
                if (StrUtil.isNotBlank(deleteField) && deleteField.equals(nbTableField.getDbName())) {
                    nbTableField.setSoftDelete(true);
                }
                //是排序字段
                String orderField = TableInfoUtils.parseOrderField(tableClazz);
                if (StrUtil.isNotBlank(orderField) && orderField.equals(nbTableField.getDbName())) {
                    nbTableField.setOrder(true);
                }*/
                nbTableFields.add(baseNbField);
            }
        }
        return nbTableFields;
    }


}
