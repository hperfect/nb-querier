package cn.hperfect.nbquerier.core.component.datasouce;

import cn.hutool.core.lang.Assert;
import com.baomidou.dynamic.datasource.AbstractRoutingDataSource;
import com.baomidou.dynamic.datasource.provider.DynamicDataSourceProvider;
import com.baomidou.dynamic.datasource.support.DdConstants;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.sql.DataSource;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/23 4:57 下午
 * @see com.baomidou.dynamic.datasource.DynamicRoutingDataSource
 */
@Log4j2
public class NbDataSource extends AbstractRoutingDataSource implements InitializingBean, DisposableBean {
    private Map<String, DataSource> dataSourceMap = new LinkedHashMap<>();
    @Setter
    private DynamicDataSourceProvider provider;

    private DataSource master;
    public NbDataSource(DynamicDataSourceProvider provider){
        this.provider = provider;
    }
    @Override
    protected DataSource determineDataSource() {
        //切换数据源->
        return master;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, DataSource> dataSources = provider.loadDataSources();
        // 添加并分组数据源
        for (Map.Entry<String, DataSource> dsItem : dataSources.entrySet()) {
            addDataSource(dsItem.getKey(), dsItem.getValue());
        }
        Assert.notNull(master, "主数据源不能为空");

    }

    @Override
    public void destroy() throws Exception {
        log.info("dynamic-datasource start closing ....");
        for (Map.Entry<String, DataSource> item : dataSourceMap.entrySet()) {
            closeDataSource(item.getKey(), item.getValue());
        }
        log.info("dynamic-datasource all closed success,bye");
    }

    private void closeDataSource(String name, DataSource dataSource) throws InvocationTargetException, IllegalAccessException {
        Class<? extends DataSource> clazz = dataSource.getClass();
        try {
            Method closeMethod = clazz.getDeclaredMethod("close");
            closeMethod.invoke(dataSource);
        } catch (NoSuchMethodException e) {
            log.warn("dynamic-datasource close the datasource named [{}] failed,", name);
        }
    }

    public synchronized void addDataSource(String ds, DataSource dataSource) {
        if (!dataSourceMap.containsKey(ds)) {
            //todo 此处省略wrapDataSource,分布式事务...
//            dataSource = wrapDataSource(ds, dataSource);
            dataSourceMap.put(ds, dataSource);
            if (ds.equals(DdConstants.MASTER)) {
                this.master = dataSource;
            }
            log.info("dynamic-datasource - load a datasource named [{}] success", ds);
        } else {
            log.warn("dynamic-datasource - load a datasource named [{}] failed, because it already exist", ds);
        }
    }


}
