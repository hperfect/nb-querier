package cn.hperfect.nbquerier.core.component.result;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 结果集处理
 *
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 10:55 上午
 */
public interface IResultSetHandler {
    /**
     * 解析成map
     *
     * @param rs
     * @return
     */
    default List<LinkedHashMap<String, Object>> toMap(ResultSet rs) throws SQLException {
        List<LinkedHashMap<String, Object>> list = new ArrayList<>();
        try {
            ResultSetMetaData md = rs.getMetaData();
            while (rs.next()) {
                LinkedHashMap<String, Object> rowData = new LinkedHashMap<>();
                int columnCount = md.getColumnCount();
                for (int i = 1; i <= columnCount; i++) {
                    rowData.put(md.getColumnName(i), rs.getObject(i));
                }
                list.add(rowData);
            }
        } finally {
            rs.close();
        }
        return list;
    }
}
