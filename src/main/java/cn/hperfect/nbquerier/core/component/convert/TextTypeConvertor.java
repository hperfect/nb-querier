package cn.hperfect.nbquerier.core.component.convert;

import cn.hutool.core.convert.Convert;
import cn.hperfect.nbquerier.core.metedata.BaseNbField;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/3 1:42 下午
 */
public enum TextTypeConvertor implements ITypeConvertor {
    /**
     * 实例
     */
    INSTANCE;

    @Override
    public String convert(BaseNbField baseNbField, Object value) {
        return Convert.toStr(value);
    }

}
