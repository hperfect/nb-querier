package cn.hperfect.nbquerier.core.component.executor;

import cn.hperfect.nbquerier.core.NbQuerier;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 10:49 上午
 */
public interface INbExecutor {
    /**
     * 执行查询
     *
     * @param tNbQuerier
     * @return
     */
    ResultSet select(NbQuerier<?> tNbQuerier);

    <T> List<LinkedHashMap<String, Object>> selectMapList(NbQuerier<T> tNbQuerier);

    int insert(NbQuerier<?> querier);
}
