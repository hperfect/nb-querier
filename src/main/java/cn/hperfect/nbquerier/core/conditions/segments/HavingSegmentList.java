package cn.hperfect.nbquerier.core.conditions.segments;

import cn.hperfect.nbquerier.core.conditions.ISqlSegment;
import cn.hperfect.nbquerier.enums.SqlKeyword;

import java.util.List;

import static java.util.stream.Collectors.joining;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 4:35 下午
 */
@SuppressWarnings("serial")
public class HavingSegmentList extends AbstractISegmentList {


    @Override
    protected boolean transformList(List<ISqlSegment> list, ISqlSegment firstSegment, ISqlSegment lastSegment) {
        if (!isEmpty()) {
            this.add(SqlKeyword.AND);
        }
        list.remove(0);
        return true;
    }

    @Override
    protected String childrenSqlSegment() {
        if (isEmpty()) {
            return EMPTY;
        }
        return this.stream().map(ISqlSegment::getSqlSegment).collect(joining(SPACE, SPACE + SqlKeyword.HAVING.getSqlSegment() + SPACE, EMPTY));
    }
}
