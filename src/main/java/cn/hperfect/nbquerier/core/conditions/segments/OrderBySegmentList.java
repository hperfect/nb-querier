package cn.hperfect.nbquerier.core.conditions.segments;

import cn.hperfect.nbquerier.core.conditions.ISqlSegment;
import cn.hperfect.nbquerier.enums.SqlKeyword;

import java.util.List;

import static java.util.stream.Collectors.joining;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 4:36 下午
 */
public class OrderBySegmentList extends AbstractISegmentList {

    @Override
    protected boolean transformList(List<ISqlSegment> list, ISqlSegment firstSegment, ISqlSegment lastSegment) {
        list.remove(0);
        final String sql = list.stream().map(ISqlSegment::getSqlSegment).collect(joining(SPACE));
        list.clear();
        list.add(() -> sql);
        return true;
    }

    @Override
    protected String childrenSqlSegment() {
        if (isEmpty()) {
            return EMPTY;
        }
        return this.stream().map(ISqlSegment::getSqlSegment).collect(joining(COMMA, SPACE + SqlKeyword.ORDER_BY.getSqlSegment() + SPACE, EMPTY));
    }
}
