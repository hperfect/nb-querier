package cn.hperfect.nbquerier.core.impl;

import cn.hperfect.nbquerier.core.NbQuerier;
import cn.hperfect.nbquerier.core.component.executor.INbExecutor;
import cn.hperfect.nbquerier.core.component.result.IResultSetHandler;
import cn.hperfect.nbquerier.core.conditions.ISqlSegment;
import cn.hperfect.nbquerier.core.conditions.segments.MergeSegments;
import cn.hperfect.nbquerier.core.metedata.NbQueryInfo;
import cn.hperfect.nbquerier.core.metedata.QueryField;
import cn.hperfect.nbquerier.core.metedata.QueryValParam;
import cn.hperfect.nbquerier.enums.NbQueryType;
import cn.hperfect.nbquerier.enums.QueryRuleEnum;
import cn.hperfect.nbquerier.enums.SqlKeyword;
import cn.hperfect.nbquerier.enums.WrapperKeyword;
import cn.hperfect.nbquerier.toolkit.LambdaUtils;
import cn.hperfect.nbquerier.toolkit.support.SFunction;
import cn.hperfect.nbquerier.toolkit.support.SerializedLambda;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * 查询构造器
 *
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 9:36 上午
 */
@Getter
@Setter
public class DefaultNbQuerier<T> implements NbQuerier<T> {
    private INbExecutor executor;
    private NbQueryInfo queryInfo;
    /**
     * 反射结果class
     */
    private Class<?> resultClass;
    /**
     * 树形结构字段
     */
    protected List<QueryField> queryFields = new ArrayList<>();
    /**
     * 参数,有序集合,类型
     */
    protected LinkedList<QueryValParam> params = new LinkedList<>();
    /**
     * 条件sql片段(复制mybatis plus)
     */
    protected MergeSegments expression = new MergeSegments();
    protected String lastSql;

    public void setQueryInfo(NbQueryInfo nbQueryInfo) {
        this.queryInfo = nbQueryInfo;
        //字段处理
        List<QueryField> list = nbQueryInfo.getFields().stream().map(i -> new QueryField(i.getQueryType(), i.getName())).collect(Collectors.toList());
        queryFields.addAll(list);
    }

    public DefaultNbQuerier(INbExecutor nbExecutor) {
        this.executor = nbExecutor;
    }

    @Override
    public String buildQuerySql() {
        String sql = StrUtil.format("SELECT * FROM {}", queryInfo.buildFormSql());
        String sqlSegment = expression.getSqlSegment();
        if (StrUtil.isNotBlank(sqlSegment)) {
            sql += " WHERE" + sqlSegment;
        }
        return sql;
    }
    /*-----------------------------------------where start-----------------------------------------*/

    /**
     * 非空条件
     *
     * @param field
     * @param value
     * @param notNull
     * @return
     */
    @Override
    public NbQuerier<T> where(String field, Object value, boolean notNull) {
        return where(field, QueryRuleEnum.EQ, value, notNull);
    }

    @Override
    public NbQuerier<T> where(String field, Object value) {
        where(field, value, false);
        return this;
    }

    @Override
    public NbQuerier<T> whereIn(String field, Collection<?> list) {
        return whereIn(field, list, false);
    }

    @Override
    public NbQuerier<T> whereIn(String field, Collection<?> list, boolean notNull) {
        Assert.isFalse(CollUtil.isEmpty(list) && notNull, "字段:" + field + "数组不能为空");
        where(field, QueryRuleEnum.IN, list, notNull);
        return this;
    }

    @Override
    public NbQuerier<T> whereNotIn(String field, Collection<?> list) {
        where(field, QueryRuleEnum.NOT_IN, list, false);
        return this;
    }

    /**
     * 匿名使用wrapper 查询(嵌套查询)
     *
     * @param
     * @return
     */
    @Override
    public NbQuerier<T> where(Consumer<NbQuerier<T>> consumer) {
        DefaultNbQuerier<T> instance = new DefaultNbQuerier<>(null);
        instance.params = this.params;
        instance.queryFields = this.queryFields;
        consumer.accept(instance);
        expression.add(WrapperKeyword.APPLY, instance.expression);
        return this;
    }

    @Override
    public NbQuerier<T> whereOr(Consumer<NbQuerier<T>> consumer) {
        return whereOr().where(consumer);
    }

    @Override
    public NbQuerier<T> whereOr() {
        expression.add(SqlKeyword.OR);
        return this;
    }

    /**
     * where 入口
     *
     * @param field
     * @param ruleEnum
     * @param value
     * @return
     */
    @Override
    public NbQuerier<T> where(String field, QueryRuleEnum ruleEnum, Object value, boolean notNull) {
        Assert.isFalse(notNull && value == null, "条件查询,字段:" + field + "值不能为空");
        Assert.notEmpty(field, "查询条件字段不能为空");
        field = StrUtil.toUnderlineCase(field);
        QueryField queryField = findQueryField(field);
        Assert.notNull(queryField, "不存在字段:{}", field);
        expression.add(queryField::getName, ruleEnum, formatVariable(queryField.getType(), value));
        return this;
    }

    @Override
    public <F> NbQuerier<T> where(SFunction<T, F> function, F value) {
        SerializedLambda resolve = LambdaUtils.resolve(function);
        String get = StrUtil.removePrefix(resolve.getImplMethodName(), "get");
        where(get, value);
        return this;
    }


    @Override
    public List<T> select() {
        executor.selectMapList(this);
        return null;
    }

    private ISqlSegment formatVariable(NbQueryType type, Object value) {
        params.push(new QueryValParam(type, value));
        return () -> "?";
    }

    public QueryField findQueryField(String name) {
        return CollUtil.findOne(this.queryFields, i -> i.getName().equals(name));
    }

    /**
     * 保存数据
     *
     * @param data
     * @return
     */
    public int save(Map<String, Object> data) {
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        /*Map<String, INbTableField> fieldMap = this.tableInfo.getFieldMap();
        saveDataBefore(data);
        //获取sql语句
        data.forEach((key, value) -> {
            key = StrUtil.toUnderlineCase(key);
            if (value != null) {
                keys.add(key);
                INbTableField iNbTableField = fieldMap.get(key);
                Assert.notNull(iNbTableField, String.format("table:%s,字段%s不存在", table, key));
                values.add(formatSql("{0}", convertToDbType(iNbTableField, value)));
            }
        });
        String keySql = String.join(",", keys);
        String valueSql = String.join(",", values);
        return nbExecutor.insert(String.format("INSERT INTO %s (%s) VALUES (%s)", this.table, keySql, valueSql), this);*/
        return executor.insert(this);
    }

    @Override
    public int save(T t) {
        Map<String, Object> map = BeanUtil.beanToMap(t);
        executor.insert(this);
        return 0;
    }

    @Override
    public List<LinkedHashMap<String, Object>> selectMap() {
        return executor.selectMapList(this);
    }

    @Override
    public String getSqlSegment() {
        String sql = expression.getSqlSegment();
        if (StrUtil.isNotBlank(lastSql)) {
            sql += lastSql;
        }
        return sql;
    }
}
