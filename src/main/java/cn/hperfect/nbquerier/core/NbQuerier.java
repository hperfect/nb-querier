package cn.hperfect.nbquerier.core;

import cn.hperfect.nbquerier.core.component.builder.INbQueryBuilder;
import cn.hperfect.nbquerier.core.conditions.ISqlSegment;
import cn.hperfect.nbquerier.core.metedata.QueryValParam;
import cn.hperfect.nbquerier.enums.QueryRuleEnum;
import cn.hperfect.nbquerier.toolkit.support.SFunction;
import cn.hutool.extra.spring.SpringUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 9:12 上午
 */
public interface NbQuerier<T> extends ISqlSegment {


    /**
     * 根据类中注解构建querier对象
     *
     * @param clazz
     * @param <T>
     * @return
     */
    static <T> NbQuerier<T> table(Class<T> clazz) {
        //获取builder 构建INbQuerier
        INbQueryBuilder builder = SpringUtil.getBean(INbQueryBuilder.class);
        return builder.build(clazz);
    }

    /**
     * 查询值相等
     *
     * @param function
     * @param value
     * @param <F>
     * @return
     */
    <F> NbQuerier<T> where(SFunction<T, F> function, F value);

    /**
     * 查询值相等
     *
     * @param field
     * @param value
     * @return
     */
    NbQuerier<T> where(String field, Object value);

    /**
     * 嵌套查询,会自动加括号
     *
     * @param
     * @return
     */
    NbQuerier<T> where(Consumer<NbQuerier<T>> consumer);

    NbQuerier<T> where(String field, Object value, boolean notNull);

    NbQuerier<T> whereIn(String field, Collection<?> list);

    NbQuerier<T> whereIn(String field, Collection<?> list, boolean notNull);

    NbQuerier<T> whereNotIn(String field, Collection<?> list);

    NbQuerier<T> whereOr(Consumer<NbQuerier<T>> consumer);

    NbQuerier<T> whereOr();

    NbQuerier<T> where(String field, QueryRuleEnum ruleEnum, Object value, boolean notNull);
    /**
     * 执行查询
     *
     * @return
     */
    List<T> select();

    /**
     * 保存
     *
     * @param t
     * @return
     */
    int save(T t);

    /**
     * 查询为map
     *
     * @return
     */
    List<LinkedHashMap<String, Object>> selectMap();

    /**
     * 构建完整的sql语句
     *
     * @return
     */
    String buildQuerySql();

    List<QueryValParam> getParams();


}
