package cn.hperfect.nbquerier.core.metedata;

import cn.hperfect.nbquerier.enums.NbFieldType;
import cn.hperfect.nbquerier.enums.NbQueryType;
import lombok.Data;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 10:05 上午
 */
@Data
public class BaseNbField {
    /**
     * 数据库字段名称(可能是函数)
     */
    protected String name;

    /**
     * 所属表名(默认为主表名)
     */
    protected String table;

    /**
     * 字段查询类型
     */
    protected NbQueryType queryType;
    protected NbFieldType fieldType;


}
