package cn.hperfect.nbquerier.core.metedata;

import cn.hperfect.nbquerier.enums.IdType;
import cn.hperfect.nbquerier.enums.NbQueryType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 主键
 *
 * @author huanxi
 * @date 2020/10/15 1:01 下午
 * @email 1355473748@qq.com
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PrimaryKey {
    private String name;
    private IdType type;
    private NbQueryType queryType;
}
