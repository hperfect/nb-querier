package cn.hperfect.nbquerier.core.metedata;

import cn.hperfect.nbquerier.enums.NbQueryType;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 4:52 下午
 */
@AllArgsConstructor
@Data
public class QueryField {

    private NbQueryType type;
    /**
     * 查询名称
     */
    private String name;


}
