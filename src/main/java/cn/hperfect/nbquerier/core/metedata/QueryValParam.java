package cn.hperfect.nbquerier.core.metedata;

import cn.hperfect.nbquerier.enums.NbQueryType;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 2:14 下午
 */
@Data
@AllArgsConstructor
public class QueryValParam {
    NbQueryType type;
    Object value;
}
