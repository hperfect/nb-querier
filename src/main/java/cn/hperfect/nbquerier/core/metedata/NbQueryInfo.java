package cn.hperfect.nbquerier.core.metedata;

import cn.hperfect.nbquerier.enums.perm.PermType;
import cn.hperfect.nbquerier.toolkit.SqlUtils;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 表或者连表信息
 *
 * @author huanxi
 * @date 2020/10/29 5:42 下午
 * @email 1355473748@qq.com
 */
@Getter
@Setter
public class NbQueryInfo {
    /**
     * 查询表名,获取子查询名称
     */
    private String sql;
    /**
     * 是否是查询
     */
    private boolean sub;

    private String alias;
    /**
     * schema
     */
    private String schema;
    /**
     * 选择数据项
     */
    private String ds;
    /**
     * 表字段
     */
    private List<? extends BaseNbField> fields;

    /**
     * 软删除字段
     */
    private BaseNbField deleteField;

    /**
     * 排序字段
     */
    private BaseNbField orderField;

    /**
     * 是否带权限
     */
    private PermType permType;


    /**
     * 主键
     */
    private PrimaryKey pk;


    /**
     * 获取主键名称
     *
     * @return
     */
    public String getPkName() {
        Assert.notNull(pk, "主键不存在");
        return this.pk.getName();
    }

    public String buildFormSql() {
        String sql = sub ? "(" + this.sql + ")" : SqlUtils.withAlias(schema, this.sql);
        if (StrUtil.isNotBlank(alias)) {
            sql = sql + " " + alias;
        }
        return sql;
    }
}
