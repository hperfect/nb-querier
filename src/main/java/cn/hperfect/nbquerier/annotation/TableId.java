package cn.hperfect.nbquerier.annotation;

import cn.hperfect.nbquerier.enums.IdType;
import cn.hperfect.nbquerier.enums.NbQueryType;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface TableId {
    String value() default "";

    IdType type() default IdType.NONE;

    NbQueryType queryType() default NbQueryType.LONG;
}
