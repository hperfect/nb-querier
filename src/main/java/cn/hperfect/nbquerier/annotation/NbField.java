package cn.hperfect.nbquerier.annotation;

import java.lang.annotation.*;

/**
 * 数据库表相关
 *
 * @author huanxi
 * @since 2016-01-23
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface NbField {

    /**
     * 数据库查询字段名
     */
    String value() default "";

    /**
     * 所属表名，连表字段
     */
    Class<?> table();

    boolean exist() default true;

}
