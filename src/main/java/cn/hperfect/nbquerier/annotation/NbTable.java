/*
 * Copyright (c) 2011-2020, baomidou (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.hperfect.nbquerier.annotation;


import cn.hperfect.nbquerier.enums.perm.PermType;

import java.lang.annotation.*;

/**
 * 数据库表
 *
 * @author huanxi
 * @since 2016-01-23
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface NbTable {

    /**
     * 实体对应的表名
     */
    String value() default "";
    String schema() default "";
    String ds() default "";

    /**
     * 是否需要验证权限
     *
     * @since 3.1.1
     */
    PermType perm() default PermType.SELF;
}
