package cn.hperfect.nbquerier.toolkit;

import cn.hutool.core.util.StrUtil;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 1:40 下午
 */
public class SqlUtils {
    /**
     * 携带别名
     *
     * @param alias
     * @param field
     * @return
     */
    public static String withAlias(String alias, String field) {
        if (StrUtil.isNotEmpty(alias)) {
            return alias + "." + field;
        }
        return field;
    }
}
