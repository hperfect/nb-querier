package cn.hperfect.nbquerier.exceptions;

import cn.hperfect.nbquerier.core.metedata.BaseNbField;
import lombok.Getter;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/3 3:46 下午
 */
@Getter
public class ConstraintException extends IllegalArgumentException {

    private final BaseNbField field;
    private final String message;
    private final String childName;

    public ConstraintException(BaseNbField field, String message) {
        this.message = message;
        this.field = field;
        this.childName = null;
    }

    public ConstraintException( BaseNbField field, String message, String childName) {
        this.field = field;
        this.message = message;
        this.childName = childName;
    }
}
