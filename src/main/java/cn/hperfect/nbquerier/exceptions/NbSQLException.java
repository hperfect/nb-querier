package cn.hperfect.nbquerier.exceptions;

import cn.hutool.core.util.StrUtil;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 2:03 下午
 */
public class NbSQLException extends RuntimeException {

    public NbSQLException(String template,Object... params) {
        super(StrUtil.format(template, params));
    }

    public NbSQLException(Throwable throwable) {
        super(throwable);
    }
}
