package cn.hperfect.nbquerier.exceptions;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/3 5:35 下午
 */
public class TypeConvertException extends Exception {
    public TypeConvertException(String message) {
        super(message);
    }
}
