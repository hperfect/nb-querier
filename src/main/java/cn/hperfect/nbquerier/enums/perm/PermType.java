package cn.hperfect.nbquerier.enums.perm;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 10:09 上午
 */
public enum PermType {
    /**
     * 所有用户,没有修改权限
     */
    ALL,
    /**
     * 用户:加上权限 字段,没有则报错
     */
    SELF,
    /**
     * 授权用户,加上子查询
     */
    AUTHORISED,
    /**
     * 自定义,必须在查询构造器中加入
     */
    INPUT;
}
