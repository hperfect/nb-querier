package cn.hperfect.nbquerier.enums;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 10:12 上午
 */
public enum IdType {
    /**
     * 自增
     */
    AUTO,
    /**
     * 不处理
     */
    NONE,
    /**
     * 手动输入
     */
    INPUT,
    /**
     * 系统复制,uuid,雪花算法
     */
    ASSIGN_ID;
}
