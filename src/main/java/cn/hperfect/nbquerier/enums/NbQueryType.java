package cn.hperfect.nbquerier.enums;


import cn.hperfect.nbquerier.core.component.convert.*;
import cn.hutool.core.util.StrUtil;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * 数据库查询类型
 *
 * @author huanxi
 * @version 1.0
 * @date 2021/3/1 8:36 下午
 */
public enum NbQueryType {
    /**
     * 字符串
     */
    TEXT(TextTypeConvertor.INSTANCE),
    /**
     * 字符串数组
     */
    TEXT_LIST(TextListTypeConvertor.INSTANCE),
    LONG(LongTypeConvertor.INSTANCE),
    LONG_LIST(LongListTypeConvertor.INSTANCE),
    INT(IntTypeConvertor.INSTANCE),
    INT_LIST(IntListTypeConvertor.INSTANCE),
    DATE(DateTypeConvertor.INSTANCE),
    DOUBLE(DoubleTypeConvertor.INSTANCE),
    BOOL(BoolTypeConvertor.INSTANCE),
    JSON(JsonTypeConvertor.INSTANCE),
    JSON_LIST(JsonTypeConvertor.INSTANCE),
    JSONB(JsonTypeConvertor.INSTANCE),
    JSONB_LIST(JsonTypeConvertor.INSTANCE),
    ENUM(TextTypeConvertor.INSTANCE),
    ENUM_LIST(TextListTypeConvertor.INSTANCE),
    /**
     * I_ENUM
     */
    MAP_ENUM(NoTypeConvertor.INSTANCE),

    MAP_ENUM_LIST(NoTypeConvertor.INSTANCE),
    /**
     * 浮点自增
     */
    AUTO_INCR_DOUBLE(DoubleTypeConvertor.INSTANCE),
    FLOAT(FloatTypeConvertor.INSTANCE),
    ;
    @Getter
    private final ITypeConvertor convert;

    NbQueryType(ITypeConvertor convert) {
        this.convert = convert;
    }


    /**
     * class 转换成 PowerTableTypeEnum
     *
     * @param fieldClazz
     * @return
     */
    public static NbQueryType convertFromClass(Class<?> fieldClazz) {

        if (String.class.equals(fieldClazz)) {
            return TEXT;
        } else if (Long.class.equals(fieldClazz)) {
            return LONG;
        } else if (Integer.class.equals(fieldClazz)) {
            return INT;
        } else if (Double.class.equals(fieldClazz)) {
            return DOUBLE;
        } else if (Boolean.class.equals(fieldClazz)) {
            return BOOL;
        } else if (LocalDateTime.class.equals(fieldClazz)) {
            return DATE;
        } else if (StrUtil.endWith(fieldClazz.getName(), "VO")) {
            return JSON;
        } else if (Map.class.isAssignableFrom(fieldClazz)) {
            return JSON;
        }
        throw new IllegalArgumentException("clazzToPowerTableType参数转换异常:" + fieldClazz.getName());
    }


}
