package cn.hperfect.nbquerier.enums;

import cn.hperfect.nbquerier.core.conditions.ISqlSegment;
import lombok.Getter;

/**
 * Query 规则 常量
 *
 * @Author Scott
 * @Date 2019年02月14日
 */
public enum QueryRuleEnum implements ISqlSegment {

    /**
     * 大于
     */
    GT(">", "gt", SqlKeyword.GT),
    /**
     * 大于等于
     */
    GE(">=", "ge", SqlKeyword.GE),
    /**
     * 小于
     */
    LT("<", "lt", SqlKeyword.LT),
    LE("<=", "le", SqlKeyword.LE),
    EQ("=", "eq", SqlKeyword.EQ),
    NE("!=", "ne", SqlKeyword.NE),
    IN("IN", "in", SqlKeyword.IN),
    NOT_IN("NOT IN", "not in", SqlKeyword.NOT_IN),
    LIKE("LIKE", "like", SqlKeyword.LIKE);

    private final String sql;
    @Getter
    private String value;
    @Getter
    private String condition;

    QueryRuleEnum(String value, String condition, ISqlSegment sqlSegment) {
        this.value = value;
        this.condition = condition;
        this.sql = sqlSegment.getSqlSegment();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @Override
    public String getSqlSegment() {
        return sql;
    }
}
