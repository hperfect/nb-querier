package cn.hperfect.nbquerier.enums;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 10:34 上午
 */
public enum NbFieldType {
    /**
     * 主键
     */
    PK,
    /**
     * 软删除
     */
    DELETE,
    /**
     * 排序
     */
    ORDER,
    /**
     * 权限字段
     */
    PERM,
    /**
     * 普通字段
     */
    NORMAL
}
