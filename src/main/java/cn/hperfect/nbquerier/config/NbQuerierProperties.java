package cn.hperfect.nbquerier.config;

import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 1:03 下午
 */
@Data
@ConfigurationProperties(prefix = NbQuerierProperties.PREFIX)
public class NbQuerierProperties {
    public static final String PREFIX = "spring.datasource.dynamic";
    /**
     * 反射class是否使用缓存
     */
    private boolean classCache = true;
    /**
     * 表名是否装换成下划线
     */
    private boolean tableNameToUnderlineCase = true;
    private boolean fieldNameToUnderlineCase = true;
}
