package cn.hperfect.nbquerier.config;

import cn.hperfect.nbquerier.core.component.builder.INbQueryBuilder;
import cn.hperfect.nbquerier.core.component.builder.impl.DefaultNbQueryBuilder;
import cn.hperfect.nbquerier.core.component.datasouce.NbDataSource;
import cn.hperfect.nbquerier.core.component.executor.DefaultNbExecutor;
import cn.hperfect.nbquerier.core.component.executor.INbExecutor;
import cn.hperfect.nbquerier.core.component.result.DefaultResultSetHandler;
import cn.hperfect.nbquerier.core.component.result.IResultSetHandler;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.dynamic.datasource.provider.DynamicDataSourceProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import javax.sql.DataSource;

/**
 * 自动化配置
 *
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 9:48 上午
 */
@Configuration
@Import({SpringUtil.class})
@EnableTransactionManagement
@EnableConfigurationProperties(NbQuerierProperties.class)
public class NbQuerierAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public INbQueryBuilder nbQueryBuilder() {
        return new DefaultNbQueryBuilder();
    }

    @Bean
    @ConditionalOnMissingBean
    public IResultSetHandler resultSetHandler() {
        return new DefaultResultSetHandler();
    }

    @Bean
    @ConditionalOnMissingBean
    public NbDataSource dataSource(DynamicDataSourceProvider dynamicDataSourceProvider) {
        return new NbDataSource(dynamicDataSourceProvider);
    }

    @Bean
    @ConditionalOnMissingBean
    public INbExecutor nbExecutor() {
        return new DefaultNbExecutor();
    }

    @Bean
    public PlatformTransactionManager txManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
