package cn.hperfect.nbquerier;

import cn.hperfect.nbquerier.core.NbQuerier;
import cn.hperfect.nbquerier.entity.TestEntity;
import cn.hperfect.nbquerier.toolkit.support.SFunction;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Function;

@SpringBootTest
@Rollback(false)
class NbQuerierApplicationTests {
    private String fieldA;

    public static void main(String[] args) throws Exception {
        /*SerializedLambda serializedLambda = doSFunction(TestEntity::getName);
        System.out.println("方法名：" + serializedLambda.getImplMethodName());
        System.out.println("类名：" + serializedLambda.getImplClass());
        System.out.println("serializedLambda：" + JSONUtil.toJsonStr(serializedLambda));*/
    }

    private static <T, R> SerializedLambda doSFunction(SFunction<T, R> func) throws Exception {
        // 直接调用writeReplace
        Method writeReplace = func.getClass().getDeclaredMethod("writeReplace");
        writeReplace.setAccessible(true);
        //反射调用
        Object sl = writeReplace.invoke(func);
        SerializedLambda serializedLambda = (java.lang.invoke.SerializedLambda) sl;
        return serializedLambda;
    }



    @Test
    void contextLoads() {
        List<LinkedHashMap<String, Object>> linkedHashMaps = NbQuerier.table(TestEntity.class)
                .where(TestEntity::getId, 1L)
                .where("name", "zs")
                .whereOr(i -> i.where("id", 123)
                        .whereIn("id", ListUtil.toList(123)))
                .selectMap();
    }

    @Test
    @Transactional(rollbackFor = Throwable.class)
    public void save() {
        TestEntity testEntity = new TestEntity(RandomUtil.randomLong(), RandomUtil.randomString(5));
        NbQuerier.table(TestEntity.class).save(testEntity);
    }

}
