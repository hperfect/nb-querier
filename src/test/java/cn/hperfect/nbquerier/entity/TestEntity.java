package cn.hperfect.nbquerier.entity;

import cn.hperfect.nbquerier.annotation.NbTable;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author huanxi
 * @version 1.0
 * @date 2021/11/24 9:43 上午
 */
@NbTable(schema = "cus_qiyq", value = "test_table")
@Data
@AllArgsConstructor
public class TestEntity {

    private Long id;

    private String name;
}
